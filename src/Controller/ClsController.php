<?php

namespace App\Controller;

use App\Entity\Cls;
use App\Form\ClsType;
use App\Repository\ClsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cls")
 */
class ClsController extends AbstractController
{
    /**
     * @Route("/", name="cls_index", methods={"GET"})
     */
    public function index(ClsRepository $clsRepository): Response
    {
        return $this->render('cls/index.html.twig', [
            'cls' => $clsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="cls_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $cl = new Cls();
        $form = $this->createForm(ClsType::class, $cl);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cl);
            $entityManager->flush();

            return $this->redirectToRoute('cls_index');
        }

        return $this->render('cls/new.html.twig', [
            'cl' => $cl,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cls_show", methods={"GET"})
     */
    public function show(Cls $cl): Response
    {
        return $this->render('cls/show.html.twig', [
            'cl' => $cl,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="cls_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Cls $cl): Response
    {
        $form = $this->createForm(ClsType::class, $cl);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cls_index');
        }

        return $this->render('cls/edit.html.twig', [
            'cl' => $cl,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cls_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Cls $cl): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cl->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cl);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cls_index');
    }
}
