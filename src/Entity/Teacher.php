<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeacherRepository")
 */
class Teacher
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $post;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Cls", mappedBy="teacher", cascade={"persist", "remove"})
     */
    private $class;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Subject")
     */
    private $subject;

    public function __construct()
    {
        $this->subject = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPost(): ?string
    {
        return $this->post;
    }

    public function setPost(string $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getClass(): ?Cls
    {
        return $this->class;
    }

    public function setClass(?Cls $class): self
    {
        $this->class = $class;

        // set (or unset) the owning side of the relation if necessary
        $newTeacher = null === $class ? null : $this;
        if ($class->getTeacher() !== $newTeacher) {
            $class->setTeacher($newTeacher);
        }

        return $this;
    }

    /**
     * @return Collection|Subject[]
     */
    public function getSubject(): Collection
    {
        return $this->subject;
    }

    public function addSubject(Subject $subject): self
    {
        if (!$this->subject->contains($subject)) {
            $this->subject[] = $subject;
        }

        return $this;
    }

    public function removeSubject(Subject $subject): self
    {
        if ($this->subject->contains($subject)) {
            $this->subject->removeElement($subject);
        }

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
