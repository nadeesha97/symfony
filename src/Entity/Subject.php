<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubjectRepository")
 */
class Subject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Teacher")
     */
    private $teacher;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Cls")
     */
    private $class;

    public function __construct()
    {
        $this->teacher = new ArrayCollection();
        $this->class = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Teacher[]
     */
    public function getTeacher(): Collection
    {
        return $this->teacher;
    }

    public function addTeacher(Teacher $teacher): self
    {
        if (!$this->teacher->contains($teacher)) {
            $this->teacher[] = $teacher;
        }

        return $this;
    }

    public function removeTeacher(Teacher $teacher): self
    {
        if ($this->teacher->contains($teacher)) {
            $this->teacher->removeElement($teacher);
        }

        return $this;
    }

    /**
     * @return Collection|Cls[]
     */
    public function getClass(): Collection
    {
        return $this->class;
    }

    public function addClass(Cls $class): self
    {
        if (!$this->class->contains($class)) {
            $this->class[] = $class;
        }

        return $this;
    }

    public function removeClass(Cls $class): self
    {
        if ($this->class->contains($class)) {
            $this->class->removeElement($class);
        }

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
