<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SchoolRepository")
 */
class School
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $town;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Teacher", cascade={"persist", "remove"})
     */
    private $principal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getPrincipal(): ?Teacher
    {
        return $this->principal;
    }

    public function setPrincipal(?Teacher $principal): self
    {
        $this->principal = $principal;

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
